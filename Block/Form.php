<?php
namespace Zaproo\Customerstatus\Block;

class Form extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Zaproo\Customerstatus\Helper\Data
     */
    protected $helper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param \Zaproo\Customerstatus\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Zaproo\Customerstatus\Helper\Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * get Save Url
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save');
    }

    public function getZstatus(){
        return $this->helper->getZstatus();
    }

    public function customerIsLoggedIn(){
        return $this->helper->customerIsLoggedIn();
    }
}
